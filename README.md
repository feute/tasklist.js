# tasklist.js
A simple task manager using React

![Sample Screenshot](https://github.com/feute/tasklist.js/raw/master/assets/images/sample.png)

## Features:
- Add tasks
- Mark and unmark tasks as completed
- Filtering
- Clear tasks by filter (all, completed, active)

## Installation
`npm` is required in order to install and run the application.

``` shell
$ npm i
```

This project uses a simple server configuration provided by `webpack-dev-server`. To run the application:

``` shell
$ npm start
```

The server listens on port 8080 by default.
