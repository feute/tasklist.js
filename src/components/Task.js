import React from 'react'
import PropTypes from 'prop-types'
import { List, ListItem } from 'material-ui/List'
import Checkbox from 'material-ui/Checkbox'

const Task = ({ onClick, done, text }) => (
  <ListItem
    leftCheckbox={<Checkbox checked={done} onClick={onClick} />}
    primaryText={text}
    style={{
      textDecoration: done ? 'line-through' : '',
      color: done ? 'gray' : ''
    }}
  />
)

Task.propTypes = {
  onClick: PropTypes.func.isRequired,
  done: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
}

export default Task
