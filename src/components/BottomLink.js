import React from 'react'
import PropTypes from 'prop-types'
import { BottomNavigationItem } from 'material-ui/BottomNavigation'

const BottomLink = ({ active, children, icon, onClick }) => (
  <BottomNavigationItem
    label={children}
    icon={icon}
    onClick={onClick}
    selected={active}
  />
)

BottomLink.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired
}

export default BottomLink
