import React from 'react'
import PropTypes from 'prop-types'
import List from 'material-ui/List'

import Task from './Task'

const Tasklist = ({ tasks, onTaskClick }) => (
  <List>
    {tasks.map((task) => (
      <Task key={task.id} {...task} onClick={() => onTaskClick(task.id)} />
    ))}
  </List>
)

Tasklist.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      done: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onTaskClick: PropTypes.func.isRequired
}

export default Tasklist
