import React from 'react'
import Paper from 'material-ui/Paper'
import { BottomNavigation } from 'material-ui/BottomNavigation'
import IconAll from 'material-ui/svg-icons/action/toc'
import IconCompleted from 'material-ui/svg-icons/action/done-all'
import IconActive from 'material-ui/svg-icons/action/history'

import FilterLink from '../containers/FilterLink'

let style = {
  position: 'fixed',
  bottom: 0
}

const Footer = () => (
  <Paper zDepth={1}>
    <BottomNavigation style={style}>
      <FilterLink filter="SHOW_ALL" icon={<IconAll />}>
        All
      </FilterLink>
      <FilterLink filter="SHOW_COMPLETED" icon={<IconCompleted />}>
        Completed
      </FilterLink>
      <FilterLink filter="SHOW_ACTIVE" icon={<IconActive />}>
        Active
      </FilterLink>
    </BottomNavigation>
  </Paper>
)

export default Footer
