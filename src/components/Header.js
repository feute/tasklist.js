import React, { Component } from 'react'
import AppBar from 'material-ui/AppBar'

import MenuList from '../containers/MenuList'

let style = {
  position: 'fixed',
  top: 0
}

class Header extends Component {
  render() {
    return (
      <AppBar
        title="Tasklist"
        style={style}
        iconElementRight={<MenuList />}
      />
    )
  }
}

export default Header
