let nextTaskId = 0

const addTask = (text) => {
  return {
    type: 'ADD_TASK',
    id: nextTaskId++,
    text
  }
}

const setFilter = (filter) => {
  return {
    type: 'SET_FILTER',
    filter
  }
}

const toggleTask = (id) => {
  return {
    type: 'TOGGLE_TASK',
    id
  }
}

const clearTasks = (filter) => {
  return {
    type: 'CLEAR_TASKS',
    filter
  }
}

export { addTask, setFilter, toggleTask, clearTasks }
