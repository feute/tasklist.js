import React, { Component } from 'react'
import { connect } from 'react-redux'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'

import { clearTasks } from '../actions'

class MenuList extends Component {
  clearAll = () => {
    this.props.dispatch(clearTasks('ALL'))
  }

  clearCompleted = () => {
    this.props.dispatch(clearTasks('COMPLETED'))
  }

  clearActive = () => {
    this.props.dispatch(clearTasks('ACTIVE'))
  }

  render() {
    return (
      <IconMenu
        iconStyle={{fill: '#ffffff'}}
        iconButtonElement={
          <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      >
        <MenuItem primaryText="Clear all" onClick={this.clearAll} />
        <MenuItem primaryText="Clear completed" onClick={this.clearCompleted} />
        <MenuItem primaryText="Clear active" onClick={this.clearActive} />
      </IconMenu>
    )
  }
}

MenuList = connect()(MenuList)

export default MenuList
