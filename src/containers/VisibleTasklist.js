import { connect } from 'react-redux'

import { toggleTask } from '../actions'
import Tasklist from '../components/Tasklist'

const getVisibleTasks = (tasks, filter) => {
  switch (filter) {
    case 'SHOW_ALL':
      return tasks
    case 'SHOW_COMPLETED':
      return tasks.filter(task => task.done)
    case 'SHOW_ACTIVE':
      return tasks.filter(task => !task.done)
  }
}

const mapStateToProps = state => {
  return {
    tasks: getVisibleTasks(state.tasks, state.filter)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTaskClick: id => {
      dispatch(toggleTask(id))
    }
  }
}

const VisibleTasklist = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Tasklist)

export default VisibleTasklist
