import React, { Component } from 'react'
import { connect } from 'react-redux'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import AddIcon from 'material-ui/svg-icons/content/add'

import { addTask } from '../actions'

const style = {
  right: 20,
  bottom: 70,
  position: 'fixed'
}

class AddTask extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      canSubmit: false,
      text: ''
    }
  }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    let text = this.state.text.trim()
    if (!text) {
      return
    }
    this.props.dispatch(addTask(text))
    this.setState({ open: false, text: '', canSubmit: false })
  }

  handleChange = (e) => {
    let text = e.target.value
    let state = {
      text,
      canSubmit: text.trim() ? true : false
    }

    this.setState(state)
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Add"
        primary={true}
        disabled={!this.state.canSubmit}
        onClick={this.handleSubmit}
      />,
    ]

    return (
      <div>
        <FloatingActionButton onClick={this.handleOpen} style={style}>
          <AddIcon />
        </FloatingActionButton>
        <Dialog
          title="Add new task"
          actions={actions}
          modal={true}
          open={this.state.open}
        >
          <form
            onSubmit={this.handleSubmit}
          >
            <TextField
              fullWidth={true}
              autoFocus={true}
              onChange={this.handleChange}
              value={this.state.text}
              name="task"
            />
          </form>
        </Dialog>
      </div>
    )
  }
}

AddTask = connect()(AddTask)

export default AddTask
