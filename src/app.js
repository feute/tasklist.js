import React, { Component } from 'react'

import Header from './components/Header'
import Tasklist from './components/Tasklist'
import AddTask from './containers/AddTask'
import VisibleTasklist from './containers/VisibleTasklist'
import Footer from './components/Footer'

let style = {
  marginTop: 60,
  marginBottom: 60
}

class App extends Component {
  render() {
    return (
      <div style={style}>
        <Header />
        <VisibleTasklist />
        <AddTask />
        <Footer />
      </div>
    )
  }
}

export default App
