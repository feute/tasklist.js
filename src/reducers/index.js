import { combineReducers } from 'redux'
import tasks from './tasks'
import filter from './filter'

const todoApp = combineReducers({
  tasks,
  filter
})

export default todoApp
