const clear = (state, filter) => {
  switch (filter) {
    case 'ALL':
      return []
    case 'ACTIVE':
      return state.filter((task) => task.done)
    case 'COMPLETED':
      return state.filter((task) => !task.done)
    default:
      return state
  }
}

const tasks = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TASK':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          done: false
        }
      ]
    case 'TOGGLE_TASK':
      return state.map((task) =>
        (task.id === action.id)
          ? { ...task, done: !task.done }
          : task
      )
    case 'CLEAR_TASKS':
      return clear(state, action.filter)
    default:
      return state
  }
}

export default tasks
